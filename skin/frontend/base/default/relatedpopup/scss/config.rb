# note: this should never truly be refernced since we are using relative assets
http_path = "/skin/frontend/base/default/relatedpopup"
css_dir = "../css"
sass_dir = "../scss"

relative_assets = true

sass_options = {:sourcemap => true}

# compass watch -e development