<?php
class Ufhs_Relatedpopup_Model_Product_Observer extends Mage_Core_Model_Abstract
{
	public function save_session_variable ($observer)
	{
		Mage::getSingleton('core/session')->setRelatedpopup($observer->getProduct()->getId());
	}
}