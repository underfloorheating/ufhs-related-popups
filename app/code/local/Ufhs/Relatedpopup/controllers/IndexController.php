<?php
class Ufhs_Relatedpopup_IndexController extends Mage_Core_Controller_Front_Action
{
	public function minicartAction()
	{
		echo Mage::app()->getLayout()
		->createBlock('relatedpopup/minicart')
		->setTemplate('relatedpopup/minicart.phtml')
		->toHtml();
	}
}