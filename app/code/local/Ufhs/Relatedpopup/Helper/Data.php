<?php
class Ufhs_Relatedpopup_Helper_Data extends Mage_Core_Helper_Abstract
{
	private function _getEmptyRelated()
	{
		return '';
	}

	public function showPopupHtml()
	{
		if ($relatedId = Mage::getSingleton('core/session')->getRelatedpopup()) {

			Mage::getSingleton('core/session')->unsetData('relatedpopup');
			$product = Mage::getModel('catalog/product')->load($relatedId);
			$related = $product->getRelatedProductIds();

			if (empty($related)) {
				$parentIds = Mage::getModel('catalog/product_type_grouped')->getParentIdsByChild($product->getId());
				if(!$parentIds) {
					$parentIds = Mage::getModel('catalog/product_type_configurable')->getParentIdsByChild($product->getId());
				}
				if(!$parentIds) {
					return $this->_getEmptyRelated();
				}
				$product = Mage::getModel('catalog/product')->load($parentIds[0]);
				$related = $product->getRelatedProductIds();
			}

			if (empty($related)) {
				return $this->_getEmptyRelated();
			}

			if ($product->getTypeId() == 'bundle') {
				$price = Mage::getModel('bundle/product_price')->getTotalPrices($product, 'min', 1);
			} else {
				$price = $product->getPrice();
			}

			$related = Mage::getModel('catalog/product')
			->getCollection()
			->addMinimalPrice()
			->addAttributeToSelect('*')
			->addAttributeToSelect('media_gallery')
			->addFieldToFilter('entity_id', ['in' => $related])
			->addFieldToFilter('visibility', Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH)
			->getData();

			$related_positions = [];
			foreach ($product->getRelatedProducts() as $item) {
				$related_positions [$item->getData('entity_id')] = $item->getData('position');
			}

			usort($related, function($a, $b) use ($related_positions){
				$aPos = $related_positions[$a['entity_id']];
				$bPos = $related_positions[$b['entity_id']];
				if ($aPos == $bPos) {
					return 0;
				}
				return ($aPos < $bPos) ? -1 : 1;
			});

			return Mage::app()->getLayout()
			->createBlock('relatedpopup/popup')
			->setData('product', $product)
			->setData('related', $related)
			->setData('price', $price)
			->setTemplate('relatedpopup/popup.phtml')
			->toHtml();
		}
	}
}