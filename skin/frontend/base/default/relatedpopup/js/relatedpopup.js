(function($){

	$(document).ready(function(){
		if ($('#rp-modal').length > 0) {

			$('button.add-to-basket').click(function(){

				var existingPreloader = (typeof show_preloader == "function" && typeof hide_preloader == "function") ? true : false;
				if (existingPreloader) {
					show_preloader();
				} else {
					$('#rp-loader').addClass('show');
				}

				var $parent = $(this).closest('.related-item');
				var $input = $parent.find('input[name="qty"]');
				$.ajax({
					url: '/checkout/cart/add',
					data: {
						form_key: $input.closest('[data-key]').data('key'),
						product: $input.data('product'),
						qty: $input.val()
					},
					method: 'post'
				}).success(function(xhr){
					$.ajax({
						url: '/relatedpopup/index/minicart',
						method: 'get'
					}).success(function(html){
						$('#rp-minicart-block').html(html);
					});
					var $oldImg = $parent.find('.image-container img');

					var imagePosition = $oldImg.position();
					var parentOffset = $oldImg.offsetParent().offset();
					var modalOffset = $('#rp-modal').offset();

					var posTop = parentOffset.top - modalOffset.top + imagePosition.top;
					var posLeft = parentOffset.left - modalOffset.left + imagePosition.left;

					var $newImg = $oldImg.clone()
						.css({
							width: $oldImg.width(),
							height: $oldImg.height(),
							top: posTop + 'px',
							left: posLeft + 'px'
						})
						.addClass('save-to-basket');
					$('#rp-modal').append($newImg);

					if (existingPreloader) {
						hide_preloader();
					} else {
						$('#rp-loader').removeClass('show');
					}

					$parent.find('.cta-container').html('<span class="success">Item added to basket</span>');
					$parent.find('.qty-box').addClass('hide');
				});

			});

			// Initialise the slick slider to display the related products
			$('ul.related-products').slick({
				infinite: false,
				speed: 300,
				slidesToShow: 4,
				slidesToScroll: 4,
				responsive: [
					{
						breakpoint: 769,
						settings: {
							slidesToShow: 3,
							slidesToScroll: 3
						}
					},
					{
						breakpoint: 601,
						settings: {
							slidesToShow: 2,
							slidesToScroll: 2
						}
					},
					{
						breakpoint: 481,
						settings: {
							slidesToShow: 1,
							slidesToScroll: 1
						}
					}
				],
				arrows: true,
				prevArrow:'<a class="slick-prev slick-arrow pull-left" data-icon="h"></a>',
				nextArrow:'<a class="slick-next slick-arrow pull-right" data-icon="i"</a>'
			});

			// Hide modal when continue shopping button is clicked.
			$('#continue-shopping, #rp-close').click(function(){
				$('#related-popup').addClass('hidden');
			});
		}
	});

}(jQuery))