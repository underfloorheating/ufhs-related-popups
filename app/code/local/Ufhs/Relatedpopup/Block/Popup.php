<?php
class Ufhs_Relatedpopup_Block_Popup extends Mage_Core_Block_Template
{
	public function getMinicartHtml()
	{
		return Mage::app()->getLayout()
		->createBlock('relatedpopup/minicart')
		->setTemplate('relatedpopup/minicart.phtml')
		->toHtml();
	}
}